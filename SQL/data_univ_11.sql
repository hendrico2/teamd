

## DUMMY DATA ##
INSERT INTO JABATAN (kode_jabatan, parent_kode_jabatan, nama_jabatan) VALUES
(1, NULL, 'Rektor USK'), 
(2, 1, 'Ketua FASILKOM USK'), 
(3, 2, 'Ketua Prodi ILMU KOMPUTER'),
(4, 3, 'Pekerja 1'),
(5, 3, 'Pekerja 2'),
(6, 4, 'Pekerja 3'),
(7, NULL, 'Rektor TU'),
(8, 7, 'Ketua FH UM'),
(9, 8, 'Ketua Prodi ILMU HUKUM'),
(10, 9, 'Pekerja 4'),
(11, 9, 'Pekerja 5'),
(12, 11, 'Pekerja 6');

INSERT INTO UNIVERSITAS (id_universitas, nama_universitas, kepala_universitas) VALUES
(11, 'UNIVERSITAS SYIAH KUALA', 1),
(13, 'UNIVERSITAS TEUKU UMAR', 3);

INSERT INTO FAKULTAS (id_fakultas, id_universitas, nama_fakultas, kepala_fakultas) VALUES
(6, 11, 'Ilmu Komputer', 2),
(1, 13, 'Hukum', 4);

INSERT INTO PRODI (id_prodi, id_fakultas, nama_prodi, kepala_prodi) VALUES
(9, 6, 'ILMU KOMPUTER', 5),
(1, 1, 'ILMU HUKUM', 6);

INSERT INTO STAF (nip, id_universitas, gaji_pokok, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, alamat, tgl_perekrutan) VALUES
('0001', 11, 15000000, 'Sinus Coaxcika', 'Jakarta', '1975-01-03', 'L', 'Islam', 'Jalan kebon kopi', '2013-05-25'),
('0002', 11, 12000000, 'Alisya Mayer', 'Jember', '1978-05-13', 'P', 'Hindu', 'Jalan kebun jeruk', '2014-01-25'),
('0003', 11, 10000000, 'John Doe', 'Bandung', '1981-02-14', 'L', 'Islam', 'Jalan gunung merapi', '2015-07-15'),
('0004', 11, 9000000, 'Troxaros Defin', 'Austria', '1972-11-25', 'L', 'Buddha', 'Jalan gunung kalem', '2011-09-12'),
('0005', 11, 9000000, 'Alex Keyguard', 'Ireland', '1971-07-23', 'L', 'Islam', 'Jalan tempat tinggal', '2013-03-25'),
('0006', 11, 9000000, 'El Fasicto', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0007', 11, 9000000, 'Orang Fakultas 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0008', 11, 9000000, 'Orang Prodi 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0009', 11, 9000000, 'Orang Pekerja 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0012', 13, 9000000, 'Orang Pekerja 4', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0013', 13, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0014', 13, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0015', 13, 9000000, 'Orang Pekerja Prodi 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0016', 13, 9000000, 'Orang Prodi 3', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0001', 13, 35000000, 'Forbagos Tredopilio', 'Quebec', '1970-03-23', 'L', 'Islam', 'Jalan jayawijaya', '2009-04-13'),
('0002', 13, 25000000, 'Oshita Bridge', 'China', '1971-03-23', 'P', 'Hindu', 'Jalan puncak raya', '2011-10-25');

INSERT INTO STAF_PEMBANTU (nip, id_universitas, kode_jabatan) VALUES
('0001', 11, 1),
('0002', 11, 2),
('0003', 11, 3),
('0007', 11, 4),
('0008', 11, 5),
('0009', 11, 6),
('0012', 13, 7),
('0013', 13, 8),
('0014', 13, 9),
('0015', 13, 10),
('0016', 13, 11),
('0001', 13, 12);

INSERT INTO STAF_DOSEN (nip, id_universitas, id_prodi, nidn, pendidikan_terakhir) VALUES
('0004', 11, 9, '000001', 'S2'),
('0005', 11, 9, '000002', 'S3'),
('0006', 11, 9, '000003', 'PhD'),
('0002', 13, 1, '000004', 'PhD');

INSERT INTO ABSENSI (nip, id_universitas, waktu_absensi) VALUES
('0001', 11, '2017-05-20 17:05:24'),
('0001', 11, '2017-05-21 17:04:14'),
('0004', 11, '2017-04-21 18:04:14'),
('0004', 11, '2017-05-22 17:04:14'),
('0004', 11, '2017-05-23 16:54:14');



## END OF SQL ##