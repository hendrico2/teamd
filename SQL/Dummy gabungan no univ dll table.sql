## SQL BEGIN ##
DROP TABLE IF EXISTS ABSENSI;
DROP TABLE IF EXISTS STAF_PEMBANTU;
DROP TABLE IF EXISTS STAF_DOSEN;
DROP TABLE IF EXISTS STAF;
DROP TABLE IF EXISTS JABATAN;

CREATE TABLE JABATAN (
  kode_jabatan INTEGER AUTO_INCREMENT NOT NULL,
  parent_kode_jabatan INTEGER,
  nama_jabatan VARCHAR(35) NOT NULL,
  IS_TETAP BOOLEAN DEFAULT TRUE,
  IS_DELETED BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (kode_jabatan),
  FOREIGN KEY (parent_kode_jabatan) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE STAF (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,
  gaji_pokok NUMERIC NOT NULL,
  nama VARCHAR(255) NOT NULL,
  tempat_lahir VARCHAR(255) NOT NULL,
  tgl_lahir DATE NOT NULL,
  jenis_kelamin CHAR(1) NOT NULL,
  agama VARCHAR(25) NOT NULL,
  alamat VARCHAR(255) NOT NULL,
  tgl_perekrutan DATE NOT NULL,
  IS_DELETED BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (nip, id_universitas)
);

CREATE TABLE STAF_PEMBANTU (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,
  kode_jabatan INTEGER NOT NULL,
  PRIMARY KEY (nip, id_universitas),
  FOREIGN KEY (nip, id_universitas) REFERENCES STAF(nip, id_universitas) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (kode_jabatan) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT,
  UNIQUE (kode_jabatan)
);

CREATE TABLE STAF_DOSEN (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,  
  id_prodi INTEGER NOT NULL,
  nidn VARCHAR(50) NOT NULL,
  pendidikan_terakhir VARCHAR(5) NOT NULL,
  PRIMARY KEY (nip, id_universitas),
  FOREIGN KEY (nip, id_universitas) REFERENCES STAF(nip, id_universitas) ON UPDATE CASCADE ON DELETE CASCADE,
  UNIQUE (nidn)
);

CREATE TABLE ABSENSI (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,
  waktu_absensi TIMESTAMP NOT NULL,
  PRIMARY KEY (nip, id_universitas, waktu_absensi),
  FOREIGN KEY (nip, id_universitas) REFERENCES STAF(nip, id_universitas) ON UPDATE CASCADE ON DELETE RESTRICT
);



## DUMMY DATA ##
INSERT INTO JABATAN (kode_jabatan, parent_kode_jabatan, nama_jabatan) VALUES
(1, NULL, 'Rektor USK'), 
(2, 1, 'Ketua FASILKOM USK'), 
(3, 2, 'Ketua Prodi ILMU KOMPUTER'),
(4, 3, 'Pekerja 1'),
(5, 3, 'Pekerja 2'),
(6, 4, 'Pekerja 3'),

(7, NULL, 'Rektor TU'),
(8, 7, 'Ketua FH UM'),
(9, 8, 'Ketua Prodi ILMU HUKUM'),
(10, 9, 'Pekerja 4'),
(11, 9, 'Pekerja 5'),
(12, 11, 'Pekerja 6');

INSERT INTO STAF (nip, id_universitas, gaji_pokok, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, alamat, tgl_perekrutan) VALUES
('0001', 11, 15000000, 'Sinus Coaxcika', 'Jakarta', '1975-01-03', 'L', 'Islam', 'Jalan kebon kopi', '2013-05-25'),
('0002', 11, 12000000, 'Alisya Mayer', 'Jember', '1978-05-13', 'P', 'Hindu', 'Jalan kebun jeruk', '2014-01-25'),
('0003', 11, 10000000, 'John Doe', 'Bandung', '1981-02-14', 'L', 'Islam', 'Jalan gunung merapi', '2015-07-15'),
('0004', 11, 9000000, 'Troxaros Defin', 'Austria', '1972-11-25', 'L', 'Buddha', 'Jalan gunung kalem', '2011-09-12'),
('0005', 11, 9000000, 'Alex Keyguard', 'Ireland', '1971-07-23', 'L', 'Islam', 'Jalan tempat tinggal', '2013-03-25'),
('0006', 11, 9000000, 'El Fasicto', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0007', 11, 9000000, 'Orang Fakultas 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0008', 11, 9000000, 'Orang Prodi 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0009', 11, 9000000, 'Orang Pekerja 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0012', 13, 9000000, 'Orang Pekerja 4', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0013', 13, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0014', 13, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0015', 13, 9000000, 'Orang Pekerja Prodi 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0016', 13, 9000000, 'Orang Prodi 3', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0001', 13, 35000000, 'Forbagos Tredopilio', 'Quebec', '1970-03-23', 'L', 'Islam', 'Jalan jayawijaya', '2009-04-13'),
('0002', 13, 25000000, 'Oshita Bridge', 'China', '1971-03-23', 'P', 'Hindu', 'Jalan puncak raya', '2011-10-25');

INSERT INTO STAF_PEMBANTU (nip, id_universitas, kode_jabatan) VALUES
('0001', 11, 1),
('0002', 11, 2),
('0003', 11, 3),
('0007', 11, 4),
('0008', 11, 5),
('0009', 11, 6),
('0012', 13, 7),
('0013', 13, 8),
('0014', 13, 9),
('0015', 13, 10),
('0016', 13, 11),
('0001', 13, 12);

INSERT INTO STAF_DOSEN (nip, id_universitas, id_prodi, nidn, pendidikan_terakhir) VALUES
('0004', 11, 9, '000001', 'S2'),
('0005', 11, 9, '000002', 'S3'),
('0006', 11, 9, '000003', 'PhD'),
('0002', 13, 1, '000004', 'PhD');

INSERT INTO ABSENSI (nip, id_universitas, waktu_absensi) VALUES
('0001', 11, '2017-05-20 17:05:24'),
('0001', 11, '2017-05-21 17:04:14'),
('0004', 11, '2017-04-21 18:04:14'),
('0004', 11, '2017-05-22 17:04:14'),
('0004', 11, '2017-05-23 16:54:14');

## DUMMY DATA TWO ##
INSERT INTO JABATAN (kode_jabatan, parent_kode_jabatan, nama_jabatan) VALUES
(13, NULL, 'Rektor UNVIERSITAS KHAIRUN'), 
(14, 13, 'Ketua Ilmu Kehidupan'), 
(15, 14, 'Ketua Teknik Makan'),
(16, NULL, 'Rektor UNIVERSITAS PAPUA'),
(17, 13, 'Ketua Ilmu Kematian'),
(18, 14, 'Ketua Teknik Minum'),
(19, 15, 'Pekerja 1'),
(20, 15, 'Pekerja 2'),
(21, 15, 'Pekerja 3'),
(22, 15, 'Pekerja 4'),
(23, 20, 'Pekerja 2 1'),
(24, 20, 'Pekerja 2 2'),
(25, 18, 'Pekerja Prodi 2 1'),
(26, 17, 'Ketua Teknik Tidur'),
(27, 16, 'Ketua Fakultas Baru'),
(28, 27, 'Ketua Prodi Baru');

INSERT INTO STAF (nip, id_universitas, gaji_pokok, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, alamat, tgl_perekrutan) VALUES
('0001', 95, 15000000, 'Sinus Coaxcika', 'Jakarta', '1975-01-03', 'L', 'Islam', 'Jalan kebon kopi', '2013-05-25'),
('0002', 95, 12000000, 'Alisya Mayer', 'Jember', '1978-05-13', 'P', 'Hindu', 'Jalan kebun jeruk', '2014-01-25'),
('0003', 95, 10000000, 'John Doe', 'Bandung', '1981-02-14', 'L', 'Islam', 'Jalan gunung merapi', '2015-07-15'),
('0004', 95, 9000000, 'Troxaros Defin', 'Austria', '1972-11-25', 'L', 'Buddha', 'Jalan gunung kalem', '2011-09-12'),
('0005', 95, 9000000, 'Alex Keyguard', 'Ireland', '1971-07-23', 'L', 'Islam', 'Jalan tempat tinggal', '2013-03-25'),
('0006', 95, 9000000, 'El Fasicto', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0007', 95, 9000000, 'Orang Fakultas 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0008', 95, 9000000, 'Orang Prodi 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0009', 95, 9000000, 'Orang Pekerja 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0010', 95, 9000000, 'Orang Pekerja 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0011', 95, 9000000, 'Orang Pekerja 3', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0012', 95, 9000000, 'Orang Pekerja 4', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0013', 95, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0014', 95, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0015', 95, 9000000, 'Orang Pekerja Prodi 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0016', 95, 9000000, 'Orang Prodi 3', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0001', 96, 35000000, 'Forbagos Tredopilio', 'Quebec', '1970-03-23', 'L', 'Islam', 'Jalan jayawijaya', '2009-04-13'),
('0002', 96, 25000000, 'Oshita Bridge', 'China', '1971-03-23', 'P', 'Hindu', 'Jalan puncak raya', '2011-10-25');

INSERT INTO STAF_PEMBANTU (nip, id_universitas, kode_jabatan) VALUES
('0001', 95, 13),
('0002', 95, 14),
('0003', 95, 15),
('0007', 95, 17),
('0008', 95, 18),
('0009', 95, 19),
('0010', 95, 20),
('0011', 95, 21),
('0012', 95, 22),
('0013', 95, 23),
('0014', 95, 24),
('0015', 95, 25),
('0016', 95, 26),
('0001', 96, 16);

INSERT INTO STAF_DOSEN (nip, id_universitas, id_prodi, nidn, pendidikan_terakhir) VALUES
('0004', 95, 10, '000005', 'S2'),
('0005', 95, 10, '000006', 'S3'),
('0006', 95, 10, '000007', 'PhD'),
('0002', 96, 13, '000008', 'PhD');

INSERT INTO ABSENSI (nip, id_universitas, waktu_absensi) VALUES
('0001', 95, '2017-05-20 17:05:24'),
('0001', 95, '2017-05-21 17:04:14'),
('0004', 95, '2017-04-21 18:04:14'),
('0004', 95, '2017-05-22 17:04:14'),
('0004', 95, '2017-05-23 16:54:14');



## END OF SQL ##