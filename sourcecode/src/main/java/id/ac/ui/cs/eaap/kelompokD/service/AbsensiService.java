package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.AbsensiModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafModel;

import java.time.YearMonth;
import java.util.Date;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
public interface AbsensiService {

    boolean newAbsensi(StafModel stafModel, Date waktu_absensi);
    boolean updAbsensi(StafModel stafModel, Date waktu_absensi, Date new_waktu_absensi);
    boolean delAbsensi(StafModel stafModel, Date waktu_absensi);

    List<AbsensiModel> getAbsensiInUniversitas(Integer id_universitas, YearMonth yearMonth);
    AbsensiModel getAbsensi(StafModel stafModel, YearMonth yearMonth);

    Integer getJumlahKehadiranDosen(String nidn, YearMonth yearMonth);
}
