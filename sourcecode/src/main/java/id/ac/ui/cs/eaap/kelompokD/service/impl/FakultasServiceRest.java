package id.ac.ui.cs.eaap.kelompokD.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.response.FakultasResponse;
import id.ac.ui.cs.eaap.kelompokD.service.FakultasService;
import id.ac.ui.cs.eaap.kelompokD.service.JabatanService;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 02-Jun-17.
 */
@Service
@Primary
public class FakultasServiceRest implements FakultasService {

    @Autowired
    JabatanService jabatanService;

    @Autowired
    StafPembantuService stafPembantuService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    @Override
    public boolean newFakultas(FakultasModel newFakultas) {
        return false;
    }

    @Override
    public boolean updFakultas(FakultasModel fakultas) {
        return false;
    }

    @Override
    public FakultasModel getFakultas(Integer id_fakultas) {
        String url = String.format("http://localhost:9090/rest/fakultas/%d", id_fakultas);
        FakultasResponse fakultasResponse = restTemplate.getForObject(url, FakultasResponse.class);

        return mapFakultasResponse(fakultasResponse);
    }

    @Override
    public List<FakultasModel> getAllFakultas(Integer id_universitas) {
        String url = String.format("http://localhost:9090/rest/%d/fakultas", id_universitas);
        List<FakultasResponse> fakultasResponses = restTemplate.getForObject(url, List.class);
        fakultasResponses = mapper.convertValue(fakultasResponses, new TypeReference<List<FakultasResponse>>() {});

        List<FakultasModel> fakultasModels = new ArrayList<>();

        for (FakultasResponse response : fakultasResponses) {
            fakultasModels.add(mapFakultasResponse(response));
        }

        return fakultasModels;
    }

    @Override
    public boolean isFakultasExist(FakultasModel fakultas) {
        return (getFakultas(fakultas.getId_fakultas()) != null);
    }

    @Override
    public JabatanModel getKetuaFakultas(Integer id_fakultas) {
        return jabatanService.getJabatan(getFakultas(id_fakultas).getKepala_fakultas());
    }

    @Override
    public List<StafPembantuModel> getAllStafPembantu(Integer id_fakultas) {
        JabatanModel ketuaFakultas = getKetuaFakultas(id_fakultas);
        Integer kode_jabatan = ketuaFakultas.getKode_jabatan();

        JabatanModel jabatanModel = jabatanService.getStructureFromRoot(kode_jabatan);

        List<StafPembantuModel> listStaf = new ArrayList<>();
        stafPembantuService.getStafData(jabatanModel, listStaf);

        return listStaf;
    }

    private FakultasModel mapFakultasResponse(FakultasResponse fakultasResponse) {
        FakultasModel fakultasModel = new FakultasModel();

        fakultasModel.setId_fakultas(fakultasResponse.getKodeFakultas());
        fakultasModel.setId_universitas(fakultasResponse.getKodeUniversitas());
        fakultasModel.setNama_fakultas(fakultasResponse.getNamaFakultas());
        fakultasModel.setKepala_fakultas(fakultasResponse.getKepalaFakultas());

        return fakultasModel;
    }
}
