package id.ac.ui.cs.eaap.kelompokD.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.ProdiModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.response.ProdiResponse;
import id.ac.ui.cs.eaap.kelompokD.service.JabatanService;
import id.ac.ui.cs.eaap.kelompokD.service.ProdiService;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 02-Jun-17.
 */
@Service
@Primary
public class ProdiServiceRest implements ProdiService {

    @Autowired
    JabatanService jabatanService;

    @Autowired
    StafPembantuService stafPembantuService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    @Override
    public boolean newProdi(ProdiModel newProdi) {
        return false;
    }

    @Override
    public boolean updProdi(ProdiModel prodi) {
        return false;
    }

    @Override
    public ProdiModel getProdi(Integer id_prodi) {
        String url = String.format("http://localhost:9090/rest/prodi/%d", id_prodi);
        ProdiResponse prodiResponse = restTemplate.getForObject(url, ProdiResponse.class);
        return mapProdiResponse(prodiResponse);
    }

    @Override
    public List<ProdiModel> getAllProdi(Integer id_fakultas) {
        String url = String.format("http://localhost:9090/rest/fakultas/%d/prodi", id_fakultas);
        List<ProdiResponse> prodiResponses = restTemplate.getForObject(url, List.class);
        prodiResponses = mapper.convertValue(prodiResponses, new TypeReference<List<ProdiResponse>>() {});

        List<ProdiModel> prodiModels = new ArrayList<>();

        for (ProdiResponse response : prodiResponses) {
            prodiModels.add(mapProdiResponse(response));
        }

        return prodiModels;
    }

    @Override
    public boolean isProdiExist(ProdiModel prodi) {
        return (getProdi(prodi.getId_prodi()) != null);
    }

    @Override
    public JabatanModel getKetuaProdi(Integer id_prodi) {
        return jabatanService.getJabatan(getProdi(id_prodi).getKepala_prodi());
    }

    @Override
    public List<StafPembantuModel> getAllStafPembantu(Integer id_prodi) {
        JabatanModel ketuaProdi = getKetuaProdi(id_prodi);
        Integer kode_jabatan = ketuaProdi.getKode_jabatan();

        JabatanModel jabatanModel = jabatanService.getStructureFromRoot(kode_jabatan);

        List<StafPembantuModel> listStaf = new ArrayList<>();
        stafPembantuService.getStafData(jabatanModel, listStaf);

        return listStaf;
    }

    private ProdiModel mapProdiResponse(ProdiResponse prodiResponse) {
        ProdiModel prodiModel = new ProdiModel();

        prodiModel.setId_prodi(prodiResponse.getKodeProdi());
        prodiModel.setId_fakultas(prodiResponse.getKodeFakultas());
        prodiModel.setNama_prodi(prodiResponse.getNamaProdi());
        prodiModel.setKepala_prodi(prodiResponse.getKepalaJabatan());

        return prodiModel;
    }
}
