package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
public interface StafDosenService {
    boolean newDosen(StafDosenModel newDosen);
    boolean updDosen(StafDosenModel dosen);
    StafDosenModel getDosen(String nidn);

    boolean isDosenExist(StafDosenModel dosen);

    List<StafDosenModel> getAllDosen(Integer id_universitas);
    List<StafDosenModel> getAllDosenInProdi(Integer id_prodi);
}
