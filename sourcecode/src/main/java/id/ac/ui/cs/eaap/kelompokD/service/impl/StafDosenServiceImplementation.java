package id.ac.ui.cs.eaap.kelompokD.service.impl;

import id.ac.ui.cs.eaap.kelompokD.dao.StafDosenMapper;
import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafDosenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@Service
public class StafDosenServiceImplementation implements StafDosenService {

    @Autowired
    StafDosenMapper stafDosenDAO;

    @Override
    public boolean newDosen(StafDosenModel newDosen) {
        return (stafDosenDAO.newDosen(newDosen) != 0);
    }

    @Override
    public boolean updDosen(StafDosenModel dosen) {
        return (stafDosenDAO.updDosen(dosen) != 0);
    }

    @Override
    public StafDosenModel getDosen(String nidn) {
        return stafDosenDAO.getDosen(nidn);
    }

    @Override
    public boolean isDosenExist(StafDosenModel dosen) {
        return (getDosen(dosen.getNidn()) != null);
    }

    @Override
    public List<StafDosenModel> getAllDosen(Integer id_universitas) {
        return stafDosenDAO.getAllDosen(id_universitas);
    }

    @Override
    public List<StafDosenModel> getAllDosenInProdi(Integer id_prodi) {
        return stafDosenDAO.getAllDosenInProdi(id_prodi);
    }
}
