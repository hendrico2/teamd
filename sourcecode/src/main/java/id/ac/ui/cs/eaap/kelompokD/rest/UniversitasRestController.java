package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.UniversitasModel;
import id.ac.ui.cs.eaap.kelompokD.response.GenericResponse;
import id.ac.ui.cs.eaap.kelompokD.service.UniversitasService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import id.ac.ui.cs.eaap.kelompokD.util.PropertyMessageUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@RestController
public class UniversitasRestController {

    @Autowired
    UniversitasService universitasService;

    @RequestMapping(value = "/rest/universitas", method = RequestMethod.GET)
    public GenericResponse<?> viewAllUniversitas() {
    	GenericResponse<List<UniversitasModel>> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(universitasService.getAllUniversitas());
        if(response.getResult().isEmpty()) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/universitas/{id_universitas}", method = RequestMethod.GET)
    public GenericResponse<?> viewUniversitas(@PathVariable Integer id_universitas) {
    	GenericResponse<UniversitasModel> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(universitasService.getUniversitas(id_universitas));
        if(response.getResult() == null) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/universitas", method = RequestMethod.POST)
    public ResponseEntity<?> addUniversitas(@RequestBody UniversitasModel newUniv, UriComponentsBuilder builder) {

        if(universitasService.isUniversitasExist(newUniv)) {
            return new ResponseEntity<>(new CustomErrorType("Universitas sudah ada."),
                                        HttpStatus.CONFLICT);
        }

        if(!universitasService.newUniversitas(newUniv)) {
            return new ResponseEntity<>(new CustomErrorType("Universitas gagal dimasukkan."),
                                        HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/universitas/{id_universitas}").buildAndExpand(newUniv.getId_universitas());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/universitas/{id_universitas}", method = RequestMethod.PUT)
    public ResponseEntity<?> updUniversitas(@PathVariable Integer id_universitas, @RequestBody UniversitasModel univ) {
        // Set user model id
        univ.setId_universitas(id_universitas);

        UniversitasModel findModel = universitasService.getUniversitas(id_universitas);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Universitas tidak ditemukan."),
                                        HttpStatus.NOT_FOUND);
        }

        if(!universitasService.updUniversitas(univ)) {
            return new ResponseEntity<>(new CustomErrorType("Universitas gagal diperbaharui."),
                                        HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(univ, HttpStatus.OK);
    }
}
