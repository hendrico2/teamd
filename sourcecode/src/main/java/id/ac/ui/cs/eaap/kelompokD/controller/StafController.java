package id.ac.ui.cs.eaap.kelompokD.controller;

import id.ac.ui.cs.eaap.kelompokD.service.StafService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by harunakaze on 25-May-17.
 */
@Controller
public class StafController {

    @Autowired
    StafService stafService;

    @RequestMapping("/{id_universitas}/universitas/chart")
    public String viewUniversitasChart(Model model, @PathVariable Integer id_universitas) {
        model.addAttribute("request_url", "/rest/" + id_universitas + "/jabatan");
        return "chart";
    }

    @RequestMapping("/{id_universitas}/fakultas/chart/{id_fakultas}")
    public String viewFakultasChart(Model model, @PathVariable Integer id_universitas, @PathVariable Integer id_fakultas) {
        model.addAttribute("request_url", "/rest/" + id_universitas + "/fakultas/chart/" + id_fakultas);
        return "chart-root";
    }

    @RequestMapping("/{id_universitas}/prodi/chart/{id_prodi}")
    public String viewProdiChart(Model model, @PathVariable Integer id_universitas, @PathVariable Integer id_prodi) {
        model.addAttribute("request_url", "/rest/" + id_universitas + "/prodi/chart/" + id_prodi);
        return "chart-root";
    }
}
