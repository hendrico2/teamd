package id.ac.ui.cs.eaap.kelompokD.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.eaap.kelompokD.dao.UniversitasMapper;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.model.UniversitasModel;
import id.ac.ui.cs.eaap.kelompokD.response.UniversitasResponse;
import id.ac.ui.cs.eaap.kelompokD.service.UniversitasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 01-Jun-17.
 */
@Service
@Primary
public class UniversitasServiceRest implements UniversitasService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    UniversitasMapper universitasDAO;

    @Override
    public boolean newUniversitas(UniversitasModel newUniv) {
        return false;
    }

    @Override
    public boolean updUniversitas(UniversitasModel univ) {
        return false;
    }

    @Override
    public UniversitasModel getUniversitas(Integer id_universitas) {
        String url = String.format("http://localhost:9090/rest/universitas/%d", id_universitas);
        UniversitasResponse universitasResponse = restTemplate.getForObject(url, UniversitasResponse.class);

        UniversitasModel universitasModel = mapUniversitasResponse(universitasResponse);
        return universitasModel;
    }

    @Override
    public boolean isUniversitasExist(UniversitasModel univ) {
        return (getUniversitas(univ.getId_universitas()) != null);
    }

    @Override
    public List<UniversitasModel> getAllUniversitas() {
        String url = String.format("http://localhost:9090/rest/universitas");
        List<UniversitasResponse> universitasResponse = restTemplate.getForObject(url, List.class);
        universitasResponse = mapper.convertValue(universitasResponse, new TypeReference<List<UniversitasResponse>>() {});

        List<UniversitasModel> universitasModels = new ArrayList<>();

        for (UniversitasResponse response : universitasResponse) {
            universitasModels.add(mapUniversitasResponse(response));
        }

        return universitasModels;
    }

    @Override
    public List<StafPembantuModel> getAllStafPembantu(Integer id_universitas) {
        return universitasDAO.getAllStafPembantu(id_universitas);
    }

    private UniversitasModel mapUniversitasResponse(UniversitasResponse universitasResponse) {
        UniversitasModel universitasModel = new UniversitasModel();

        universitasModel.setId_universitas(universitasResponse.getKodeUniv());
        universitasModel.setNama_universitas(universitasResponse.getNamaUniv());
        universitasModel.setKepala_universitas(universitasResponse.getKepalaJabatan());

        return universitasModel;
    }
}
