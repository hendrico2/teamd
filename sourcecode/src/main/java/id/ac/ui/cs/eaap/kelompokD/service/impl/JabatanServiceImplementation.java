package id.ac.ui.cs.eaap.kelompokD.service.impl;

import id.ac.ui.cs.eaap.kelompokD.dao.JabatanMapper;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.service.JabatanService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
@Service
public class JabatanServiceImplementation implements JabatanService {

    @Autowired
    JabatanMapper jabatanDAO;

    @Override
    public boolean newJabatan(JabatanModel data) {
        if (jabatanDAO.newJabatan(data) != 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updJabatan(JabatanModel data) {
        if(jabatanDAO.updJabatan(data) != 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean delJabatan(Integer kode_jabatan) {
        if(jabatanDAO.delJabatan(kode_jabatan) != 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean isJabatanExist(JabatanModel jabatan) {
        return (getJabatan(jabatan.getKode_jabatan()) != null);
    }

    @Override
    public List<JabatanModel> getAllJabatan(Integer id_universitas) {
        List<JabatanModel> results = jabatanDAO.getAllJabatan(id_universitas);
        return results;
    }

    @Override
    public JabatanModel getJabatan(Integer kode_jabatan) {
        JabatanModel jabatanModel = jabatanDAO.getJabatan(kode_jabatan);
        return jabatanModel;
    }

    @Override
    public JabatanModel getStructureFromRoot(Integer kode_jabatan) {
        return jabatanDAO.getStructureFromRoot(kode_jabatan);
    }
}
