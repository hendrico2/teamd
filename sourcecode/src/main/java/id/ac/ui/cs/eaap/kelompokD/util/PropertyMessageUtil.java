package id.ac.ui.cs.eaap.kelompokD.util;

import java.io.InputStream;
import java.util.Properties;

public class PropertyMessageUtil {

	public static Properties getMessageProperties() {
		Properties props = new Properties();
		InputStream input = null;
		
		try {
			String filename = "messages.properties";
    		input = PropertyMessageUtil.class.getClassLoader().getResourceAsStream(filename);
			if (input != null) {
				props.load(input);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return props;
	}
	
}
