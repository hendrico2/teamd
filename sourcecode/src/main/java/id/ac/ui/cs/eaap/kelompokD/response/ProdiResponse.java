package id.ac.ui.cs.eaap.kelompokD.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiResponse {
	private int kodeProdi;
	private String namaProdi;
	private int kodeFakultas;
	private int kepalaJabatan;
	//
	private String namaFakultas;
}
