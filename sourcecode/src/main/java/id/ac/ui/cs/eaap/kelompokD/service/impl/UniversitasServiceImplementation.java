package id.ac.ui.cs.eaap.kelompokD.service.impl;

import id.ac.ui.cs.eaap.kelompokD.dao.UniversitasMapper;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.model.UniversitasModel;
import id.ac.ui.cs.eaap.kelompokD.service.UniversitasService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Service
public class UniversitasServiceImplementation implements UniversitasService {

    @Autowired
    UniversitasMapper universitasDAO;

    @Override
    public boolean newUniversitas(UniversitasModel newUniv) {
        return (universitasDAO.newUniversitas(newUniv) != 0);
    }

    @Override
    public boolean updUniversitas(UniversitasModel univ) {
        return (universitasDAO.updUniversitas(univ) != 0);
    }

    @Override
    public UniversitasModel getUniversitas(Integer id_universitas) {
        return universitasDAO.getUniversitas(id_universitas);
    }

    @Override
    public boolean isUniversitasExist(UniversitasModel univ) {
        return (getUniversitas(univ.getId_universitas()) != null);
    }

    @Override
    public List<UniversitasModel> getAllUniversitas() {
        return universitasDAO.getAllUniversitas();
    }

    @Override
    public List<StafPembantuModel> getAllStafPembantu(Integer id_universitas) {
        return universitasDAO.getAllStafPembantu(id_universitas);
    }
}
