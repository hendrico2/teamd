package id.ac.ui.cs.eaap.kelompokD.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniversitasResponse {
	private int kodeUniv;
	private String namaUniv;
	private String urlUniv;
	private int kepalaJabatan;
	private List<FakultasResponse> listFakultas;
}