package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.ProdiModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@Mapper
public interface ProdiMapper {

    @Insert("INSERT INTO PRODI (id_fakultas, nama_prodi, kepala_prodi) VALUES " +
            "(#{id_fakultas}, #{nama_prodi}, #{kepala_prodi})")
    @Options(useGeneratedKeys = true, keyProperty = "id_prodi")
    int newProdi(ProdiModel newProdi);

    @Update("UPDATE prodi SET id_fakultas = #{id_fakultas}, nama_prodi = #{nama_prodi}, kepala_prodi = #{kepala_prodi} " +
            "WHERE id_prodi = #{id_prodi}")
    int updProdi(ProdiModel prodi);

    @Select("SELECT * FROM prodi WHERE id_prodi = #{id_prodi}")
    ProdiModel getProdi(@Param("id_prodi") Integer id_prodi);

    @Select("SELECT * FROM prodi WHERE id_fakultas = #{id_fakultas}")
    List<ProdiModel> getAllProdi(@Param("id_fakultas") Integer id_fakultas);

    @Select("SELECT * " +
            "FROM prodi " +
            "JOIN jabatan ON jabatan.kode_jabatan = prodi.kepala_prodi " +
            "JOIN staf_pembantu USING (kode_jabatan) " +
            "JOIN staf ON staf.nip = staf_pembantu.nip AND staf.id_universitas = staf_pembantu.id_universitas " +
            "WHERE id_prodi = #{id_prodi}")
    JabatanModel getKetuaProdi(@Param("id_prodi") Integer id_prodi);
}
