package id.ac.ui.cs.eaap.kelompokD.service.impl;

import id.ac.ui.cs.eaap.kelompokD.model.*;
import id.ac.ui.cs.eaap.kelompokD.service.AbsensiService;
import id.ac.ui.cs.eaap.kelompokD.service.FakultasService;
import id.ac.ui.cs.eaap.kelompokD.service.PayrollService;
import id.ac.ui.cs.eaap.kelompokD.service.ProdiService;
import id.ac.ui.cs.eaap.kelompokD.service.StafDosenService;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;
import id.ac.ui.cs.eaap.kelompokD.service.UniversitasService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Service
public class PayrollServiceImplementation implements PayrollService {

    private final static Double MINIMUM_KEHADIRAN = 4.0;

    @Autowired
    StafPembantuService StafPembantuService;

    @Autowired
    StafDosenService stafDosenService;

    @Autowired
    UniversitasService universitasService;

    @Autowired
    FakultasService fakultasService;

    @Autowired
    ProdiService prodiService;

    @Autowired
    AbsensiService absensiService;

    @Override
    public PayrollModel getPayrollReportPembantu(StafPembantuModel staf, YearMonth yearMonth) {
        AbsensiModel dataAbsensi = absensiService.getAbsensi(staf, yearMonth);
        StafModel dataStaff = StafPembantuService.getPembantu(dataAbsensi.getNip(), dataAbsensi.getId_universitas());
        Double gajiPokok = dataStaff.getGaji_pokok();

        Double totalGaji = getTotalGaji(gajiPokok, dataAbsensi.getJumlah_hadir());

        PayrollModel dataPayroll = new PayrollModel(dataStaff, totalGaji);

        return dataPayroll;
    }

    @Override
    public PayrollModel getPayrollReportDosen(StafDosenModel stafDosen, YearMonth yearMonth) {
        Integer jumlahKehadiran = absensiService.getJumlahKehadiranDosen(stafDosen.getNidn(), yearMonth);
        Double gajiPokok = stafDosen.getGaji_pokok();
        Double totalGaji =  getTotalGaji(gajiPokok, jumlahKehadiran);

        PayrollModel dataPayroll = new PayrollModel(stafDosen, totalGaji);

        return dataPayroll;
    }

    @Override
    public List<PayrollModel> getDosenPayrollReportForProdi(Integer id_prodi, YearMonth yearMonth) {
        List<PayrollModel> payroll = new ArrayList<>();
        List<StafDosenModel> listStafDosen = stafDosenService.getAllDosenInProdi(id_prodi);

        for (StafDosenModel stafDosenModel : listStafDosen) {
            payroll.add(getPayrollReportDosen(stafDosenModel, yearMonth));
        }
        return payroll;
    }

    @Override
    public List<PayrollModel> getAllPayrollReportForProdi(Integer id_prodi, YearMonth yearMonth) {
        List<PayrollModel> payroll = new ArrayList<>();

        List<StafPembantuModel> listStafPembantu = prodiService.getAllStafPembantu(id_prodi);

        for (StafPembantuModel stafPembantuModel : listStafPembantu) {
            payroll.add(getPayrollReportPembantu(stafPembantuModel, yearMonth));
        }

        List<StafDosenModel> listStafDosen = stafDosenService.getAllDosenInProdi(id_prodi);

        for (StafDosenModel stafDosenModel : listStafDosen) {
            payroll.add(getPayrollReportDosen(stafDosenModel, yearMonth));
        }

        return payroll;
    }

    @Override
    public List<PayrollModel> getAllPayrollReportForFakultas(Integer id_fakultas, YearMonth yearMonth) {
        List<PayrollModel> payroll = new ArrayList<>();

        List<StafPembantuModel> listStafPembantu = fakultasService.getAllStafPembantu(id_fakultas);

        for (StafPembantuModel stafPembantuModel : listStafPembantu) {
            payroll.add(getPayrollReportPembantu(stafPembantuModel, yearMonth));
        }
        return payroll;
    }

    @Override
    public List<PayrollModel> getAllPayrollReportForUniversitas(Integer id_universitas, YearMonth yearMonth) {
        List<PayrollModel> payroll = new ArrayList<>();

        List<StafPembantuModel> listStafPembantu = universitasService.getAllStafPembantu(id_universitas);

        for (StafPembantuModel stafPembantuModel : listStafPembantu) {
            payroll.add(getPayrollReportPembantu(stafPembantuModel, yearMonth));
        }
        return payroll;
    }

    private Double getTotalGaji(Double gajiPokok, Integer jumlahKehadiran) {
        Double gaji_pokok = gajiPokok;

        Double nilai_kehadiran = (jumlahKehadiran / MINIMUM_KEHADIRAN);
        nilai_kehadiran = Math.min(1.0, nilai_kehadiran);

        Double presentasi_ketidakhadiran = 1.0 - (nilai_kehadiran);

        return gaji_pokok - (gaji_pokok * presentasi_ketidakhadiran);
    }
}
