package id.ac.ui.cs.eaap.kelompokD.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FakultasResponse {
	private int kodeFakultas;
	private String namaFakultas;
	private int kodeUniversitas;
	private int kepalaFakultas;
	private List<ProdiResponse> listProdi;
	//
	private String namaUniv;
}
