package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by harunakaze on 12-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JabatanModel {
    private Integer kode_jabatan;
    private Integer parent_kode_jabatan;
    private String nama_jabatan;
    private boolean is_tetap;
    private boolean IS_DELETED;
    private StafPembantuModel data_staff;
    private List<JabatanModel> bawahan_langsung;
}
