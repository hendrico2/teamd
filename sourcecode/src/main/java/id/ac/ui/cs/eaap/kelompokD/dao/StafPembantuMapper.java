package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Mapper
public interface StafPembantuMapper {


    @Insert("INSERT INTO STAF_PEMBANTU (nip, id_universitas, kode_jabatan) VALUES " +
            "(#{nip}, #{id_universitas}, #{kode_jabatan})")
    int newPembantu(StafPembantuModel newPembantu);

    @Update("UPDATE staf_pembantu SET kode_jabatan = #{kode_jabatan} " +
            "WHERE nip = #{nip} AND #{id_universitas}")
    int updPembantu(StafPembantuModel pembantu);

    @Select("SELECT * FROM staf_pembantu JOIN staf USING (nip, id_universitas) " +
            "WHERE is_deleted = FALSE AND " +
            "staf_pembantu.nip = #{nip} AND staf_pembantu.id_universitas = #{id_universitas}")
    StafPembantuModel getPembantu(@Param("nip") String nip, @Param("id_universitas") Integer id_universitas);

    @Select("SELECT * FROM staf_pembantu JOIN staf USING (nip, id_universitas) " +
            "WHERE is_deleted = FALSE AND staf_pembantu.id_universitas = #{id_universitas}")
    List<StafPembantuModel> getAllPembantu(@Param("id_universitas") Integer id_universitas);

    @Select("SELECT * FROM staf_pembantu JOIN staf USING (nip, id_universitas) " +
            "WHERE is_deleted = FALSE AND " +
            "staf_pembantu.kode_jabatan = #{kode_jabatan}")
    StafPembantuModel getPembantuFromJabatan(@Param("kode_jabatan") Integer kode_jabatan);
}
