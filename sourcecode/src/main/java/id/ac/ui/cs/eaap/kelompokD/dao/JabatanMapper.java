package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
@Mapper
public interface JabatanMapper {
    @Insert("INSERT INTO JABATAN (parent_kode_jabatan, nama_jabatan, is_tetap) VALUES " +
            "(#{parent_kode_jabatan},#{nama_jabatan}, #{is_tetap})")
    @Options(useGeneratedKeys = true, keyProperty = "kode_jabatan")
    int newJabatan(JabatanModel data);

    @Update("UPDATE JABATAN SET " +
            "parent_kode_jabatan = #{parent_kode_jabatan}, " +
            "nama_jabatan = #{nama_jabatan}, " +
            "is_tetap = #{is_tetap} " +
            "WHERE kode_jabatan = #{kode_jabatan}")
    int updJabatan(JabatanModel data);

    @Update("UPDATE JABATAN SET IS_DELETED = TRUE WHERE kode_jabatan = #{kode_jabatan}")
    int delJabatan(@Param("kode_jabatan") Integer kode_jabatan);

    @Select("SELECT * FROM jabatan WHERE kode_jabatan = #{kode_jabatan} AND IS_DELETED = FALSE")
    @Results(value = {
            @Result(property="kode_jabatan", column="kode_jabatan"),
            @Result(property="parent_kode_jabatan", column="parent_kode_jabatan"),
            @Result(property = "data_staff", column = "kode_jabatan",
                    javaType = StafPembantuModel.class,
                    one = @One(select = "getStafData")),
            @Result(property = "bawahan_langsung", column = "kode_jabatan",
                    javaType = List.class,
                    many = @Many(select = "getOneLevelBawahan"))
    })
    JabatanModel getJabatan(@Param("kode_jabatan") Integer kode_jabatan);

    @Select("SELECT * FROM jabatan JOIN staf_pembantu USING (kode_jabatan) WHERE IS_DELETED = FALSE AND id_universitas = #{id_universitas}")
    @Results(value = {
            @Result(property="kode_jabatan", column="kode_jabatan"),
            @Result(property="parent_kode_jabatan", column="parent_kode_jabatan"),
            @Result(property = "data_staff", column = "kode_jabatan",
                    javaType = StafPembantuModel.class,
                    one = @One(select = "getStafData"))
    })
    List<JabatanModel> getAllJabatan(@Param("id_universitas") Integer id_universitas);

    @Select("SELECT * FROM jabatan WHERE IS_DELETED = FALSE AND kode_jabatan = #{kode_jabatan}")
    @Results(value = {
            @Result(property="kode_jabatan", column="kode_jabatan"),
            @Result(property="parent_kode_jabatan", column="parent_kode_jabatan"),
            @Result(property = "data_staff", column = "kode_jabatan",
                    javaType = StafPembantuModel.class,
                    one = @One(select = "getStafData")),
            @Result(property = "bawahan_langsung", column = "kode_jabatan",
                    javaType = List.class,
                    many = @Many(select = "getBawahanLangsung"))
    })
    JabatanModel getStructureFromRoot(@Param("kode_jabatan") Integer kode_jabatan);

    @Select("SELECT * FROM jabatan WHERE IS_DELETED = FALSE AND parent_kode_jabatan = #{parent_kode_jabatan}")
    @Results(value = {
            @Result(property="kode_jabatan", column="kode_jabatan"),
            @Result(property="parent_kode_jabatan", column="parent_kode_jabatan"),
            @Result(property = "data_staff", column = "kode_jabatan",
                    javaType = StafPembantuModel.class,
                    one = @One(select = "getStafData")),
            @Result(property = "bawahan_langsung", column = "kode_jabatan",
                    javaType = List.class,
                    many = @Many(select = "getBawahanLangsung"))
    })
    List<JabatanModel> getBawahanLangsung(@Param("parent_kode_jabatan") Integer parent_kode_jabatan);


    @Select("SELECT * FROM staf_pembantu JOIN staf USING(nip, id_universitas) WHERE kode_jabatan = #{kode_jabatan}")
    StafPembantuModel getStafData(@Param("kode_jabatan") Integer kode_jabatan);

    @Select("SELECT * FROM jabatan WHERE IS_DELETED = FALSE AND parent_kode_jabatan = #{parent_kode_jabatan}")
    @Results(value = {
            @Result(property="kode_jabatan", column="kode_jabatan"),
            @Result(property="parent_kode_jabatan", column="parent_kode_jabatan"),
            @Result(property = "data_staff", column = "kode_jabatan",
                    javaType = StafPembantuModel.class,
                    one = @One(select = "getStafData"))
    })
    List<JabatanModel> getOneLevelBawahan(@Param("parent_kode_jabatan") Integer parent_kode_jabatan);
}
