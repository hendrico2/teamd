package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.response.GenericResponse;
import id.ac.ui.cs.eaap.kelompokD.service.FakultasService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import id.ac.ui.cs.eaap.kelompokD.util.PropertyMessageUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 28-May-17.
 */
@RestController
public class FakultasRestController {

    @Autowired
    FakultasService fakultasService;

    @RequestMapping(value = "/rest/{id_universitas}/fakultas", method = RequestMethod.GET)
    public GenericResponse<?> viewAllFakultas(@PathVariable Integer id_universitas) {
    	GenericResponse<List<FakultasModel>> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(fakultasService.getAllFakultas(id_universitas));
        if(response.getResult().isEmpty()) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/fakultas/{id_fakultas}", method = RequestMethod.GET)
    public GenericResponse<?> viewFakultas(@PathVariable Integer id_fakultas) {
    	GenericResponse<FakultasModel> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(fakultasService.getFakultas(id_fakultas));
        if(response.getResult() == null) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/fakultas", method = RequestMethod.POST)
    public ResponseEntity<?> addFakultas(@RequestBody FakultasModel newFakultas, UriComponentsBuilder builder) {

        if(fakultasService.isFakultasExist(newFakultas)) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!fakultasService.newFakultas(newFakultas)) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/fakultas/{id_fakultas}").buildAndExpand(newFakultas.getId_fakultas());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/fakultas/{id_fakultas}", method = RequestMethod.PUT)
    public ResponseEntity<?> updFakultas(@PathVariable Integer id_fakultas, @RequestBody FakultasModel fakultas) {
        // Set user model id
        fakultas.setId_fakultas(id_fakultas);

        FakultasModel findModel = fakultasService.getFakultas(id_fakultas);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!fakultasService.updFakultas(fakultas)) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(fakultas, HttpStatus.OK);
    }
}
