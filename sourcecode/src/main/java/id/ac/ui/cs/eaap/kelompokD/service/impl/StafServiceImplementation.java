package id.ac.ui.cs.eaap.kelompokD.service.impl;


import id.ac.ui.cs.eaap.kelompokD.dao.StafMapper;
import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
@Service
public class StafServiceImplementation implements StafService {

    @Autowired
    StafMapper stafDAO;

    @Override
    public boolean newStaf(StafModel data) {
        if (stafDAO.newStaf(data) != 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean updStaf(StafModel data) {
        if (stafDAO.updStaf(data) != 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean delStaf(String nip, Integer id_universitas) {
        if (stafDAO.delStaf(nip, id_universitas)!= 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isStafExist(StafModel staf) {
        return (getStaf(staf.getNip(), staf.getId_universitas()) != null);
    }

    @Override
    public StafModel getStaf(String nip, Integer id_universitas) {
        StafModel result = stafDAO.getStaf(nip, id_universitas);
        return result;
    }

    @Override
    public List<StafModel> getAllStaf(Integer id_universitas) {
        List<StafModel> results = stafDAO.getAllStaf(id_universitas);
        return results;
    }
}
