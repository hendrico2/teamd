package id.ac.ui.cs.eaap.kelompokD.service.impl;

import id.ac.ui.cs.eaap.kelompokD.dao.StafPembantuMapper;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Service
public class StafPembantuServiceImplementation implements StafPembantuService {

    @Autowired
    StafPembantuMapper stafPembantuDAO;

    @Override
    public boolean newPembantu(StafPembantuModel newPembantu) {
        return (stafPembantuDAO.newPembantu(newPembantu) != 0);
    }

    @Override
    public boolean updPembantu(StafPembantuModel pembantu) {
        return (stafPembantuDAO.updPembantu(pembantu) != 0);
    }

    @Override
    public boolean isPembantuExist(StafPembantuModel pembantu) {
        return (getPembantu(pembantu.getNip(), pembantu.getId_universitas()) != null);
    }

    @Override
    public List<StafPembantuModel> getAllPembantu(Integer id_universitas) {
        return stafPembantuDAO.getAllPembantu(id_universitas);
    }

    @Override
    public StafPembantuModel getPembantu(String nip, Integer id_universitas) {
        return stafPembantuDAO.getPembantu(nip, id_universitas);
    }

    @Override
    public StafPembantuModel getPembantuFromJabatan(Integer kode_jabatan) {
        return stafPembantuDAO.getPembantuFromJabatan(kode_jabatan);
    }

    @Override
    public void getStafData(JabatanModel jabatan, List<StafPembantuModel> listStaf) {
        listStaf.add(getPembantuFromJabatan(jabatan.getKode_jabatan()));
        List<JabatanModel> daftarJabatan = jabatan.getBawahan_langsung();

        for (JabatanModel jabatanModel : daftarJabatan) {
            getStafData(jabatanModel, listStaf);
        }
    }
}
