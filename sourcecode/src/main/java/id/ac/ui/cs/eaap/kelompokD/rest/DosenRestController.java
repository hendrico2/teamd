package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;
import id.ac.ui.cs.eaap.kelompokD.response.GenericResponse;
import id.ac.ui.cs.eaap.kelompokD.service.StafDosenService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import id.ac.ui.cs.eaap.kelompokD.util.PropertyMessageUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@RestController
public class DosenRestController {

    @Autowired
    StafDosenService stafDosenService;

    @RequestMapping(value = "/rest/dosen/{nidn}", method = RequestMethod.GET)
    public GenericResponse<?> getDosen(@PathVariable String nidn) {
        GenericResponse<StafDosenModel> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(stafDosenService.getDosen(nidn));
        if(response.getResult() == null) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/{id_universitas}/dosen/viewall", method = RequestMethod.GET)
    public GenericResponse<?> getDosenAll(@PathVariable Integer id_universitas) {
        GenericResponse<List<StafDosenModel>> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(stafDosenService.getAllDosen(id_universitas));
        if(response.getResult().isEmpty()) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/{id_universitas}/dosen/prodi/viewall/{id_prodi}", method = RequestMethod.GET)
    public GenericResponse<?> getDosenAllInProdi(@PathVariable Integer id_prodi) {
        GenericResponse<List<StafDosenModel>> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        response.setResult(stafDosenService.getAllDosenInProdi(id_prodi));
        if(response.getResult().isEmpty()) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        }
        return response;
    }

    @RequestMapping(value = "/rest/dosen", method = RequestMethod.POST)
    public ResponseEntity<?> addDosen(@RequestBody StafDosenModel newDosen, UriComponentsBuilder builder) {

        if(stafDosenService.isDosenExist(newDosen)) {
            return new ResponseEntity<>(new CustomErrorType("Dosen sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!stafDosenService.newDosen(newDosen)) {
            return new ResponseEntity<>(new CustomErrorType("Dosen gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/dosen/{nidn}").buildAndExpand(newDosen.getNidn());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/dosen/{nidn}", method = RequestMethod.PUT)
    public ResponseEntity<?> updDosen(@PathVariable String nidn, @RequestBody StafDosenModel dosen) {
    	GenericResponse<StafDosenModel> response = new GenericResponse<>();
        response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.0"));
        response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.0"));
        // Set user model id
        dosen.setNidn(nidn);
        // get current dosen data
        StafDosenModel findModel = stafDosenService.getDosen(nidn);
        // check if no dosen available with given nidn
        if(findModel == null) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.10003"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.10003"));
        } // check if dosen data successfully updated
        else if(!stafDosenService.updDosen(dosen)) {
        	response.setMessage(PropertyMessageUtil.getMessageProperties().getProperty("response.code.message.11006"));
            response.setStatus(PropertyMessageUtil.getMessageProperties().getProperty("response.code.status.11006"));
        } // if dosen already available, and dosen successfully updated
        else{
        	response.setResult(dosen);
        }

        return new ResponseEntity<>(dosen, HttpStatus.OK);
    }
}
