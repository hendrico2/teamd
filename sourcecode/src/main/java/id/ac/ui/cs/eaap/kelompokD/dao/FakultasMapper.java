package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@Mapper
public interface FakultasMapper {

    @Insert("INSERT INTO FAKULTAS (id_universitas, nama_fakultas, kepala_fakultas) VALUES " +
            "(#{id_universitas}, #{nama_fakultas}, #{kepala_fakultas})")
    @Options(useGeneratedKeys = true, keyProperty = "id_fakultas")
    int newFakultas(FakultasModel newFakultas);

    @Update("UPDATE fakultas SET " +
            "id_universitas = #{id_universitas}, " +
            "nama_fakultas = #{nama_fakultas}, " +
            "kepala_fakultas = #{kepala_fakultas} " +
            "WHERE id_fakultas = #{id_fakultas}")
    int updFakultas(FakultasModel fakultas);

    @Select("SELECT * FROM fakultas WHERE id_fakultas = #{id_fakultas}")
    FakultasModel getFakultas(@Param("id_fakultas") Integer id_fakultas);

    @Select("SELECT * FROM fakultas WHERE id_universitas = #{id_universitas}")
    List<FakultasModel> getAllFakultas(@Param("id_universitas") Integer id_universitas);

    @Select("SELECT * " +
            "FROM fakultas " +
            "JOIN jabatan ON jabatan.kode_jabatan = fakultas.kepala_fakultas " +
            "JOIN staf_pembantu USING (kode_jabatan) " +
            "JOIN staf ON staf.nip = staf_pembantu.nip AND staf.id_universitas = staf_pembantu.id_universitas " +
            "WHERE id_fakultas = #{id_fakultas}")
    JabatanModel getKetuaFakultas(@Param("id_fakultas") Integer id_fakultas);
}
