package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@Mapper
public interface StafDosenMapper {

    @Insert("INSERT INTO STAF_DOSEN (nip, id_universitas, id_prodi, nidn, pendidikan_terakhir) VALUES " +
            "(#{nip}, #{id_universitas}, #{id_prodi}, #{nidn}, #{pendidikan_terakhir})")
    int newDosen(StafDosenModel newDosen);

    @Update("UPDATE staf_dosen SET id_prodi = #{id_prodi}, pendidikan_terakhir = #{pendidikan_terakhir} " +
            "WHERE nidn = #{nidn}")
    int updDosen(StafDosenModel dosen);

    @Select("SELECT * FROM staf_dosen JOIN staf USING (nip, id_universitas) WHERE is_deleted = FALSE AND nidn = #{nidn}")
    StafDosenModel getDosen(String nidn);

    @Select("SELECT * FROM staf_dosen JOIN staf USING (nip, id_universitas) WHERE is_deleted = FALSE AND id_universitas = #{id_universitas}")
    List<StafDosenModel> getAllDosen(Integer id_universitas);

    @Select("SELECT * FROM staf_dosen JOIN staf USING (nip, id_universitas) WHERE is_deleted = FALSE AND id_prodi = #{id_prodi}")
    List<StafDosenModel> getAllDosenInProdi(Integer id_prodi);
}
