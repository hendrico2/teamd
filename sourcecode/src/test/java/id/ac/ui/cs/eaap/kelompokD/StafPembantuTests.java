package id.ac.ui.cs.eaap.kelompokD;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.service.JabatanService;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;
import id.ac.ui.cs.eaap.kelompokD.service.StafService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CO2 on 5/31/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StafPembantuTests {
    @Autowired
    StafPembantuService stafPembantuService;

    @Autowired
    StafService stafService;

    @Autowired
    JabatanService jabatanService;

    @Test
    public void testCreateStafPembantu() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date tgl_lahir = new Date();
        Date tgl_perekrutan = new Date();
        try {
            tgl_lahir = df.parse("1975-01-03");
            tgl_perekrutan = df.parse("2013-05-25");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Buat staf baru
        StafModel stafModel = new StafModel("9999", 1, 15000000.0, "Sinus Coaxcika",
                "Jakarta", tgl_lahir, "L", "Islam", "Jalan kebon kopi",
                tgl_perekrutan, false);

        Assert.assertNull("Staf sudah ada", stafService.getStaf(stafModel.getNip(),
                stafModel.getId_universitas()));

        stafService.newStaf(stafModel);
        Assert.assertNotNull("Staf gagal dimasukkan", stafService.getStaf(stafModel.getNip(),
                stafModel.getId_universitas()));

        // Buat jabatan baru
        JabatanModel jabatanModel = new JabatanModel();
        jabatanModel.setNama_jabatan("Jabatan Baru");

        jabatanService.newJabatan(jabatanModel);

        Assert.assertNotNull("Jabatan gagal dimasukkan",
                jabatanService.getJabatan(jabatanModel.getKode_jabatan()));

        // Angkat jadi staf pembantu
        StafPembantuModel stafPembantuModel = new StafPembantuModel(jabatanModel.getKode_jabatan());
        stafPembantuModel.setNip(stafModel.getNip());
        stafPembantuModel.setId_universitas(stafModel.getId_universitas());

        Assert.assertNull("Staf Pembantu sudah ada", stafPembantuService.getPembantu(stafModel.getNip(),
                stafModel.getId_universitas()));
        stafPembantuService.newPembantu(stafPembantuModel);
        Assert.assertNotNull("Staf Pembantu gagal ditambahkan",
                stafPembantuService.getPembantu(stafModel.getNip(), stafModel.getId_universitas()));
    }

    @Test
    public void testUpdateStafPembantu() {
        // Buat jabatan baru
        JabatanModel jabatanModel = new JabatanModel();
        jabatanModel.setNama_jabatan("Jabatan Baru");

        jabatanService.newJabatan(jabatanModel);

        Assert.assertNotNull("Jabatan gagal dimasukkan",
                jabatanService.getJabatan(jabatanModel.getKode_jabatan()));

        // Persiapan update staf
        StafPembantuModel stafPembantuModel = new StafPembantuModel(7);
        stafPembantuModel.setNip("0009");
        stafPembantuModel.setId_universitas(1);

        Assert.assertNotNull("Data gagal ditemukan",
                stafPembantuService.getPembantu(stafPembantuModel.getNip(), stafPembantuModel.getId_universitas()));

        stafPembantuModel.setKode_jabatan(jabatanModel.getKode_jabatan());
        stafPembantuService.updPembantu(stafPembantuModel);
        Assert.assertEquals("Data gagal diupdate", jabatanModel.getKode_jabatan(),
                stafPembantuService.getPembantu(stafPembantuModel.getNip(),
                        stafPembantuModel.getId_universitas()).getKode_jabatan());
    }
}
