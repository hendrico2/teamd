package id.ac.ui.cs.eaap.kelompokD;

import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.service.FakultasService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by CO2 on 5/30/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class FakultasServiceTests {
    @Autowired
    FakultasService fakultasService;

    @Test
    public void testCreateFakultas() {
        FakultasModel fakultas = new FakultasModel(0, 1, "Fakultas Dummy", 2);
        Assert.assertNull("Gagal - Fakultas sudah ada", fakultasService.getFakultas(fakultas.getId_fakultas()));
        fakultasService.newFakultas(fakultas);
        Assert.assertNotNull("Gagal - Data fakultas baru gagal dimasukkan", fakultasService.getFakultas(fakultas.getId_fakultas()));
    }

    @Test
    public void testUpdateFakultas() {
        FakultasModel fakultas = new FakultasModel(1, 1, "Ilmu Komputer", 2);
        Assert.assertNotNull("Data gagal ditemukan", fakultasService.getFakultas(1));
        fakultas.setNama_fakultas("Sistem Informasi");
        fakultasService.updFakultas(fakultas);
        Assert.assertEquals("Data gagal diupdate", "Sistem Informasi", fakultasService.getFakultas(1).getNama_fakultas());
    }

}
