package id.ac.ui.cs.eaap.kelompokD;

import id.ac.ui.cs.eaap.kelompokD.model.ProdiModel;
import id.ac.ui.cs.eaap.kelompokD.service.ProdiService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by CO2 on 5/30/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ProdiServiceTests {
    @Autowired
    ProdiService prodiService;

    @Test
    public void testCreateProdi() {
        ProdiModel prodi = new ProdiModel(0, 1, "Prodi Dummy", 5);
        Assert.assertNull("Gagal - Prodi sudah ada", prodiService.getProdi(prodi.getId_prodi()));
        prodiService.newProdi(prodi);
        Assert.assertNotNull("Gagal - Prodi baru tidak dapat dimasukkan ke dalam DB", prodiService.getProdi(prodi.getId_prodi()));
    }

    @Test
    public void testUpdateProdi() {
        ProdiModel prodi = new ProdiModel(2, 1, "Prodi 2", 6);
        Assert.assertNotNull("Gagal - Prodi tidak ditemukan", prodiService.getProdi(prodi.getId_prodi()));
        prodi.setNama_prodi("Prodi Dummy");
        prodiService.updProdi(prodi);
        Assert.assertEquals("Gagal - Prodi gagal diupdate", "Prodi Dummy", prodiService.getProdi(prodi.getId_prodi()).getNama_prodi());
    }
}
