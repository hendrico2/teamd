package id.ac.ui.cs.eaap.kelompokD;

import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafDosenService;
import id.ac.ui.cs.eaap.kelompokD.service.StafService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CO2 on 5/31/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StafDosenTests {
    @Autowired
    StafDosenService stafDosenService;

    @Autowired
    StafService stafService;

    @Test
    public void testCreateStafDosen() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date tgl_lahir = new Date();
        Date tgl_perekrutan = new Date();
        try {
            tgl_lahir = df.parse("1975-01-03");
            tgl_perekrutan = df.parse("2013-05-25");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Buat staf baru
        StafModel stafModel = new StafModel("9999", 1, 15000000.0, "Sinus Coaxcika",
                "Jakarta", tgl_lahir, "L", "Islam", "Jalan kebon kopi",
                tgl_perekrutan, false);

        Assert.assertNull("Staf sudah ada", stafService.getStaf(stafModel.getNip(),
                stafModel.getId_universitas()));

        stafService.newStaf(stafModel);
        Assert.assertNotNull("Staf gagal dimasukkan", stafService.getStaf(stafModel.getNip(),
                stafModel.getId_universitas()));

        // Angkat jadi dosen
        StafDosenModel stafDosenModel = new StafDosenModel(2, "000009", "S3");
        stafDosenModel.setNip("9999");
        stafDosenModel.setId_universitas(1);

        Assert.assertNull("Staf Dosen sudah ada", stafDosenService.getDosen("000009"));
        stafDosenService.newDosen(stafDosenModel);
        Assert.assertNotNull("Staf Dosen gagal ditambahkan", stafDosenService.getDosen("000009"));
    }

    @Test
    public void testUpdateStafDosen() {
        StafDosenModel stafDosenModel = new StafDosenModel(1, "000003", "PhD");;
        Assert.assertNotNull("Data gagal ditemukan", stafDosenService.getDosen("000003"));
        stafDosenModel.setPendidikan_terakhir("S1");
        stafDosenService.updDosen(stafDosenModel);
        Assert.assertEquals("Data gagal diupdate", "S1",
                stafDosenService.getDosen("000003").getPendidikan_terakhir());
    }
}
