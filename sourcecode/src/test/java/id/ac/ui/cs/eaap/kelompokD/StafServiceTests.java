package id.ac.ui.cs.eaap.kelompokD;

import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Get-Up on 5/30/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StafServiceTests {
    @Autowired
    StafService stafService;

    @Test
    public void testCreateNewStaf() {
        StafModel staf = new StafModel("Dummy", 1, new Double("10000"),
                "Dummy", "Dummy Place", new Date(1496102400),
                "L", "Dummy Religion", "Dummy Location",
                new Date(1496102400), false);
        Assert.assertNull("Gagal - data staf dummy sudah ada",
                stafService.getStaf(staf.getNip(), staf.getId_universitas()));
        stafService.newStaf(staf);
        Assert.assertNotNull("Gagal - data staf dummy gagal di-insert ke dalam db",
                stafService.getStaf(staf.getNip(), staf.getId_universitas()));
    }

    @Test
    public void testUpdateStaf() {
        StafModel staf = stafService.getStaf("0001",1);
        Assert.assertNotNull("Gagal - data staf tidak ditemukan",staf);
        staf.setNama("Dummy");
        stafService.updStaf(staf);
        Assert.assertEquals("Gagal - data staf dummy gagal di-update",
                "Dummy",
                stafService.getStaf(staf.getNip(), staf.getId_universitas()).getNama());
    }
}
